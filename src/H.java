import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

import static java.lang.Math.*;

public class H {

    public static Heuristic getHeuristicByCode(int code, Collection<M.Node> nodes) {
        Heuristic instance;
        switch (code) {
            case 1:
                instance = new ZeroHeuristic();
                break;
            case 2:
                instance = new ManhattanHeuristic();
                break;
            case 3:
                instance = new DiagonalHeuristic();
                break;
            default:
                return null;
        }
        instance.addGoals(nodes);
        return instance;
    }

    public static abstract class Heuristic {
        protected final SortedSet<M.Node> goals = new TreeSet<M.Node>();

        public final void addGoals(Collection<M.Node> nodes) {
            goals.addAll(nodes);
        }

        public abstract double getHeurisctic(M.Node node);
    }

    private static class ZeroHeuristic extends Heuristic {
        @Override
        public double getHeurisctic(M.Node node) {
            return 0;
        }
    }

    private static class ManhattanHeuristic extends Heuristic {
        @Override
        public double getHeurisctic(M.Node node) {
            double result = Double.POSITIVE_INFINITY;
            for (M.Node goal : goals) {
                double goalHeur = abs(goal.getX() - node.getX()) + abs(goal.getY() - node.getY());
                result = goalHeur < result ? goalHeur : result;
            }
            return result;
        }
    }

    private static class DiagonalHeuristic extends Heuristic {
        @Override
        public double getHeurisctic(M.Node node) {
            double result = Double.POSITIVE_INFINITY;
            for (M.Node goal : goals) {
                double goalHeur = sqrt(pow(goal.getX() - node.getX(), 2) + pow(goal.getY() - node.getY(), 2));
                result = goalHeur < result ? goalHeur : result;
            }
            return result;
        }
    }

}
