import java.util.*;

public class A {

    public static Algorithm getAlgorithmByCode(int code, H.Heuristic heuristic) {
        switch (code) {
            case 1:
                return new BFSAlgorithm();
            case 2:
                return new DFSAlgorithm();
            case 3:
                return new UCSAlgorithm();
            case 4:
                return new GreedyAlgorithm(heuristic);
            case 5:
                return new AStarAlgorithm(heuristic);
        }
        return null;
    }

    public static abstract class Algorithm {
        protected final H.Heuristic heuristic;

        protected Algorithm(H.Heuristic heuristic) {
            this.heuristic = heuristic;
        }

        public abstract List<M.Node> solve(M.Problem problem);

    }

    private static final class BFSAlgorithm extends Algorithm {
        protected Deque<M.Node> nodeDeque;
        private Map<M.Node, M.Node> visitMap;

        protected BFSAlgorithm() {
            super(null);
        }

        private void markAndAdd(M.Node node, M.Node parent) {
            visitMap.put(node, parent);
            nodeDeque.addLast(node);
        }

        @Override
        public List<M.Node> solve(M.Problem problem) {
            nodeDeque = new ArrayDeque<M.Node>();
            visitMap = new HashMap<M.Node, M.Node>();

            markAndAdd(problem.getStart(), null);
            while (!nodeDeque.isEmpty()) {
                final M.Node node = nodeDeque.removeFirst();

                if (problem.getGoals().contains(node)) {
                    List<M.Node> path = new LinkedList<M.Node>();
                    M.Node pathIter = node;
                    while (pathIter != null) {
                        path.add(0, pathIter);
                        pathIter = visitMap.get(pathIter);
                    }
                    return path;
                }

                for (M.Node childNode : node.getNeighbors().keySet()) {
                    if (!visitMap.containsKey(childNode)) {
                        markAndAdd(childNode, node);
                    }
                }
            }

            return null;
        }
    }

    private static final class DFSAlgorithm extends Algorithm {
        protected Deque<M.Node> nodeDeque;
        private Map<M.Node, M.Node> visitMap;

        protected DFSAlgorithm() {
            super(null);
        }

        private void markAndAdd(M.Node node, M.Node parent) {
            visitMap.put(node, parent);
            nodeDeque.addFirst(node);
        }

        @Override
        public List<M.Node> solve(M.Problem problem) {
            nodeDeque = new ArrayDeque<M.Node>();
            visitMap = new HashMap<M.Node, M.Node>();

            markAndAdd(problem.getStart(), null);
            while (!nodeDeque.isEmpty()) {
                final M.Node node = nodeDeque.removeFirst();

                if (problem.getGoals().contains(node)) {
                    List<M.Node> path = new LinkedList<M.Node>();
                    M.Node pathIter = node;
                    while (pathIter != null) {
                        path.add(0, pathIter);
                        pathIter = visitMap.get(pathIter);
                    }
                    return path;
                }

                for (M.Node childNode : node.getNeighbors().descendingKeySet()) {
                    if (!visitMap.containsKey(childNode)) {
                        markAndAdd(childNode, node);
                    }
                }
            }

            return null;
        }
    }

    private static final class UCSAlgorithm extends Algorithm {
        private PriorityQueue<M.CostedNode> priorityQueue;
        private Map<M.CostedNode, M.CostedNode> costMap;
        private Set<M.Node> visitSet;

        public UCSAlgorithm() {
            super(null);
        }

        private void markAndAdd(M.CostedNode costedNode, M.CostedNode costedParent) {
            costMap.put(costedNode, costedParent);
            visitSet.add(costedNode.getNode());
            priorityQueue.add(costedNode);
        }

        @Override
        public List<M.Node> solve(M.Problem problem) {
            priorityQueue = new PriorityQueue<M.CostedNode>();
            costMap = new HashMap<M.CostedNode, M.CostedNode>();
            visitSet = new HashSet<M.Node>();

            markAndAdd(new M.CostedNode(problem.getStart(), 0, 0), null);
            while (!priorityQueue.isEmpty()) {
                final M.CostedNode costedNode = priorityQueue.remove();

                if (problem.getGoals().contains(costedNode.getNode())) {
                    List<M.Node> path = new LinkedList<M.Node>();
                    M.CostedNode pathIter = costedNode;
                    while (pathIter != null) {
                        path.add(0, pathIter.getNode());
                        pathIter = costMap.get(pathIter);
                    }
                    return path;
                }

                for (Map.Entry<M.Node, Integer> childEntry : costedNode.getNode().getNeighbors().entrySet()) {
                    final M.CostedNode costedChild = new M.CostedNode(childEntry.getKey(),
                            costedNode.getCost() + childEntry.getValue(), 0);

                    if (!visitSet.contains(costedChild.getNode())) {
                        markAndAdd(costedChild, costedNode);
                    }
                }
            }

            return null;
        }

    }

    private static final class GreedyAlgorithm extends Algorithm {
        private PriorityQueue<M.CostedNode> priorityQueue;
        private Map<M.CostedNode, M.CostedNode> costMap;
        private Set<M.Node> visitSet;

        protected GreedyAlgorithm(H.Heuristic heuristic) {
            super(heuristic);
        }


        private void markAndAdd(M.CostedNode costedNode, M.CostedNode costedParent) {
            costMap.put(costedNode, costedParent);
            visitSet.add(costedNode.getNode());
            priorityQueue.add(costedNode);
        }

        @Override
        public List<M.Node> solve(M.Problem problem) {
            priorityQueue = new PriorityQueue<M.CostedNode>();
            costMap = new HashMap<M.CostedNode, M.CostedNode>();
            visitSet = new HashSet<M.Node>();

            markAndAdd(new M.CostedNode(problem.getStart(), 0,
                    heuristic.getHeurisctic(problem.getStart())), null);
            while (!priorityQueue.isEmpty()) {
                final M.CostedNode costedNode = priorityQueue.remove();

                if (problem.getGoals().contains(costedNode.getNode())) {
                    List<M.Node> path = new LinkedList<M.Node>();
                    M.CostedNode pathIter = costedNode;
                    while (pathIter != null) {
                        path.add(0, pathIter.getNode());
                        pathIter = costMap.get(pathIter);
                    }
                    return path;
                }

                for (Map.Entry<M.Node, Integer> childEntry : costedNode.getNode().getNeighbors().entrySet()) {
                    final M.CostedNode costedChild = new M.CostedNode(childEntry.getKey(), 0,
                            heuristic.getHeurisctic(childEntry.getKey()));

                    if (!visitSet.contains(costedChild.getNode())) {
                        markAndAdd(costedChild, costedNode);
                    }
                }
            }

            return null;
        }

    }

    public static final class AStarAlgorithm extends Algorithm {
        private PriorityQueue<M.CostedNode> priorityQueue;
        private Map<M.CostedNode, M.CostedNode> costMap;
        private Set<M.Node> visitSet;

        protected AStarAlgorithm(H.Heuristic heuristic) {
            super(heuristic);
        }


        private void markAndAdd(M.CostedNode costedNode, M.CostedNode costedParent) {
            costMap.put(costedNode, costedParent);
            visitSet.add(costedNode.getNode());
            priorityQueue.add(costedNode);
        }

        @Override
        public List<M.Node> solve(M.Problem problem) {
            priorityQueue = new PriorityQueue<M.CostedNode>();
            costMap = new HashMap<M.CostedNode, M.CostedNode>();
            visitSet = new HashSet<M.Node>();

            markAndAdd(new M.CostedNode(problem.getStart(), 0,
                    heuristic.getHeurisctic(problem.getStart())), null);
            while (!priorityQueue.isEmpty()) {
                final M.CostedNode costedNode = priorityQueue.remove();

                if (problem.getGoals().contains(costedNode.getNode())) {
                    List<M.Node> path = new LinkedList<M.Node>();
                    M.CostedNode pathIter = costedNode;
                    while (pathIter != null) {
                        path.add(0, pathIter.getNode());
                        pathIter = costMap.get(pathIter);
                    }
                    return path;
                }

                for (Map.Entry<M.Node, Integer> childEntry : costedNode.getNode().getNeighbors().entrySet()) {
                    final M.CostedNode costedChild = new M.CostedNode(childEntry.getKey(),
                            costedNode.getCost() + childEntry.getValue(),
                            heuristic.getHeurisctic(childEntry.getKey()));

                    if (!visitSet.contains(costedChild.getNode())) {
                        markAndAdd(costedChild, costedNode);
                    }
                }
            }

            return null;
        }

    }

}
