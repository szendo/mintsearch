import java.io.*;
import java.util.*;

public class IO {

    public static class InputParser {

        private enum ReadState {
            NODES, EDGES, START, GOALS, HEUR, ALGO
        }

        private static boolean isSeparator(String line) {
            return line.isEmpty() || line.startsWith("#");
        }

        public static M.Problem parseInput(File iFile) throws IOException {
            final SortedMap<Integer, M.Node> nodes = new TreeMap<Integer, M.Node>();
            M.Node startNode = null;
            final Set<M.Node> goalNodes = new HashSet<M.Node>();
            H.Heuristic heur = null;
            M.Problem result = null;

            boolean prevAtSeparator, atSeparator = true;
            String line;

            ReadState state = ReadState.NODES;
            final BufferedReader reader = new BufferedReader(new FileReader(iFile));
            while (null != (line = reader.readLine())) {
                prevAtSeparator = atSeparator;
                atSeparator = isSeparator(line);
                if (state == ReadState.NODES) {
                    if (atSeparator) {
                        if (!prevAtSeparator) state = ReadState.EDGES;
                    }
                    else {
                        String[] args = line.split("\t");
                        final int id = Integer.parseInt(args[0]);
                        nodes.put(id, new M.Node(id, Integer.parseInt(args[2]), Integer.parseInt(args[3])));
                    }

                }
                else if (state == ReadState.EDGES) {
                    if (atSeparator) {
                        if (!prevAtSeparator) state = ReadState.START;
                    }
                    else {
                        String[] args = line.split("\t");
                        final int id1 = Integer.parseInt(args[0]);
                        final int id2 = Integer.parseInt(args[1]);
                        final int w = Integer.parseInt(args[2]);
                        nodes.get(id1).addNeighbor(nodes.get(id2), w);
                        nodes.get(id2).addNeighbor(nodes.get(id1), w);
                    }
                }
                else if (state == ReadState.START) {
                    if (atSeparator) {
                        if (!prevAtSeparator) state = ReadState.GOALS;
                    }
                    else {
                        startNode = nodes.get(Integer.parseInt(line));
                    }
                }
                else if (state == ReadState.GOALS) {
                    if (atSeparator) {
                        if (!prevAtSeparator) state = ReadState.HEUR;
                    }
                    else {
                        final int id = Integer.parseInt(line);
                        goalNodes.add(nodes.get(id));
                    }
                }
                else if (state == ReadState.HEUR) {
                    if (atSeparator) {
                        if (!prevAtSeparator) state = ReadState.ALGO;
                    }
                    else {
                        heur = H.getHeuristicByCode(Integer.parseInt(line), goalNodes);
                    }
                }
                else if (state == ReadState.ALGO) {
                    if (atSeparator) {
                        if (!prevAtSeparator) state = null;
                    }
                    else {
                        final A.Algorithm algorithm = A.getAlgorithmByCode(Integer.parseInt(line), heur);
                        result = new M.Problem(startNode, goalNodes, algorithm);

                    }
                }
            }
            reader.close();

            return result;
        }

    }

    public static class OutputGenerator {

        public static void generateOutput(File oFile, List<M.Node> solution) throws IOException {
            final PrintWriter writer = new PrintWriter(new FileWriter(oFile));
            if (!solution.isEmpty()) {
                writer.print(solution.get(0).getId());
                for (int i = 1; i < solution.size(); i++) {
                    writer.println();
                    writer.print(solution.get(i).getId());
                }
            }
            writer.flush();
            writer.close();
        }

    }

}
