import java.io.File;
import java.io.IOException;

public class Search {

    public static void main(String[] args) throws IOException {
        if (args.length == 2) {
            final M.Problem problem = IO.InputParser.parseInput(new File(args[0]));
            IO.OutputGenerator.generateOutput(new File(args[1]), problem.solve());
        }
    }

}
