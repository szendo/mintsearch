import java.util.*;

public class M {

    public static final class Problem {
        private final Node start;
        private final Set<Node> goals;
        private final A.Algorithm algorithm;

        public Problem(Node start, Set<Node> goals, A.Algorithm algorithm) {
            this.start = start;
            this.goals = Collections.unmodifiableSet(goals);
            this.algorithm = algorithm;
        }

        public Node getStart() {
            return start;
        }

        public Set<Node> getGoals() {
            return goals;
        }

        public List<Node> solve() {
            return algorithm.solve(this);
        }

    }

    public static final class Node implements Comparable<Node> {
        private final int id, x, y;

        private final NavigableMap<Node, Integer> neighbors;

        public Node(int id, int x, int y) {
            this.id = id;
            this.x = x;
            this.y = y;
            this.neighbors = new TreeMap<Node, Integer>();
        }

        public int getId() {
            return id;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        public NavigableMap<Node, Integer> getNeighbors() {
            return neighbors;
        }

        public void addNeighbor(Node n, int w) {
            neighbors.put(n, w);
        }

        @Override
        public boolean equals(Object o) {
            return (this == o) || (o instanceof Node) && (this.compareTo((Node) o) == 0);
        }

        @Override
        public int hashCode() {
            return id;
        }

        @Override
        public int compareTo(Node o) {
            return this.id - o.id;
        }

    }

    public static class CostedNode implements Comparable<CostedNode> {
        private final Node node;
        private final int cost;
        private final double heur;

        public CostedNode(Node node, int cost, double heur) {
            this.node = node;
            this.cost = cost;
            this.heur = heur;
        }

        public Node getNode() {
            return node;
        }

        public int getCost() {
            return cost;
        }

        private double getScore() {
            return cost + heur;
        }

        @Override
        public boolean equals(Object o) {
            return (this == o) || (o instanceof CostedNode) && (this.compareTo((CostedNode) o) == 0);
        }

        @Override
        public int hashCode() {
            int result;
            result = node != null ? node.hashCode() : 0;
            result = 31 * result + cost;
            final long temp = heur != +0.0d ? Double.doubleToLongBits(heur) : 0L;
            result = 31 * result + (int) (temp ^ (temp >>> 32));
            return result;
        }

        @Override
        public int compareTo(CostedNode o) {
            if (Math.abs(this.getScore() - o.getScore()) < 1e-6) {
                return this.node.compareTo(o.node);
            }
            else {
                return Double.compare(this.getScore(), o.getScore());
            }
        }

    }

}
