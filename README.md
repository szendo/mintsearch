Keres�si feladat
===

Jelen h�zi feladat c�lja az el�ad�son elhangzott keres�si algoritmusok implement�ci�j�n kereszt�l az egyes algoritmusokkal kapcsolatban gyakorlati tapasztalatok szerz�se. A sz�mos k�l�nb�z� algoritmus implement�ci�ja seg�t kidombor�tani a m�dszerek hasonl�s�gait �s k�l�nbs�geit. Az algoritmusokat Java nyelven (1.6-os JDK alatt) kell implement�lni. A feladat bead�sa forr�sk�d alakban t�rt�nik. Az alkalmaz�s f�oszt�ly�nak a "Search" nevet kell viselnie, de ezen k�v�l tetsz�leges sz�m� tov�bbi saj�t oszt�ly felhaszn�lhat�. A program indul�sa sor�n k�t parancssori param�tert v�rjon:

1. A bemeneti f�jl neve (teljes el�r�si �ttal)
2. A kimeneti f�jl neve (teljes el�r�si �ttal)

A be- �s kimeneti f�jlok form�tum�t az al�bbiakban ismertetj�k.

Implement�land� algoritmusok �s heurisztik�k
---
Az el�ad�sokon megismert algoritmusok k�z�l az al�bbiak implement�l�sa sz�ks�ges:

1. Sz�less�gi keres�s
2. M�lys�gi keres�s
3. Egyenletes k�lts�g� keres�s
4. Moh� keres�s (sz�less�gi jelleggel)
5. A* keres�s

Az el�ad�sokon megismert heurisztik�k k�z�l az al�bbiak implement�l�sa sz�ks�ges:

1. Konstans 0 heurisztika
2. Manhattan (h�zt�mb) t�vols�g
3. L�gvonalbeli t�vols�g

Az algoritmusok �s heurisztik�k r�szletes defin�ci�ja megtal�lhat� az el�ad�sanyagban illetve a tank�nyvben. Az algoritmusok bizonyos esetekben nemdeterminisztikus m�k�d�st mutatnak, p�ld�ul a moh� algoritmus k�t azonos heurisztik�j� csom�pont k�z�l tetsz�legesen v�laszthat. Az ehhez hasonl� nemdeterminizmust a k�nny� g�pi ellen�rizhet�s�g �rdek�ben szeretn�nk kiz�rni, ez�rt alapvet� szab�ly, hogy ahol egy algoritmus ilyen d�nt�si helyzetbe ker�l, ott mindig a numerikus azonos�t� alapj�n n�vekv� sorrendben kell a csom�pontokat kifejteni. A tesztfuttat�sok sor�n lesznek olyan esetek, amikor a csom�pontok k�z�l t�bb is c�lk�nt szerepel. Ilyenkor az algoritmusnak az els� c�lcsom�pont megtal�l�s�t k�vet�en meg kell �llnia.

A bemeneti form�tum
---
A keres�si gr�fot egy sz�veges f�jl defini�lja. A f�jl hat blokkra tagol�dik, mely blokkokat �res sorok, illetve "#" jellel kezd�d� megjegyz�s sorok v�lasztanak el. Az egyes blokkok form�tuma az al�bbi:

1. Csom�pontok - minden sorban 4 darab tabul�torral elv�lasztott mez� tal�lhat�, melyeknek jelent�se az al�bbi:
    1. A csom�pont numerikus azonos�t�ja: a kimenetben ezzel azonos�that�k a csom�pontok.
    2. A csom�pont c�mk�je: tetsz�leges sz�veg, amit az opcion�lis grafikus fel�let megjelen�thet a csom�pont mellett a k�nnyebb �ttekinthet�s�g �rdek�ben.
    3. A csom�pont poz�ci�j�nak X koordin�t�ja: nem negat�v eg�sz �rt�k.
    4. A csom�pont poz�ci�j�nak Y koordin�t�ja: nem negat�v eg�sz �rt�k.
2. �lek - minden sorban 3 darab tabul�torral elv�lasztott mez� tal�lhat�:
    1. Az �l egyik v�gpontj�nak numerikus azonos�t�ja.
    2. Az �l m�sik v�gpontj�nak numerikus azonos�t�ja.
    3. Az �l k�lts�ge: pozit�v eg�sz �rt�k.
    4. Az �lek minden esetben k�tir�ny�ak, teh�t a bemeneti f�jlban csak egyszer jelennek meg, de mindk�t ir�nyban azonos k�lts�ggel ig�nybe vehet�ek.
3. Kiindul� csom�pont - ebben a blokkban mind�ssze egyetlen sor szerepel, mely a kiindul� csom�pont numerikus azonos�t�j�t tartalmazza.
4. C�lcsom�pontok - ebben a blokkban legal�bb egy sor szerepel. Minden sor egy-egy c�lcsom�pont numerikus azonos�t�j�t tartalmazza.
5. Heurisztika - ebben a blokkban egyetlen sor szerepel egyetlen numerikus �rt�kkel (1-3), mely a felhaszn�land� heurisztik�t azonos�tja (a k�dol�st l�sd fent, az implement�land� heurisztik�kn�l)
6. Keres�si algoritmus - ebben a blokkban egyetlen sor szerepel egyetlen numerikus �rt�kkel (1-5), mely a futtatand� algoritmust azonos�tja (a k�dol�st l�sd fent, az implement�land� algoritmusokn�l)

A fentiek �rtelm�ben a nem inform�lt keres�sek eset�n is defini�lt egy heurisztika a megfelel� mez�ben, melyet ilyenkor az adott keres�si algoritmus figyelmen k�v�l hagy.

A kimeneti form�tum
---
A kimeneti form�tum soronk�nt egy-egy numerikus csom�pont azonos�t�t tartalmaz. Az els� sorban a kiindul� csom�pont azonos�t�ja szerepel, az utols� sorban a megtal�lt c�lcsom�pont azonos�t�ja, a k�zbens� sorokban a c�lhoz vezet� �ton szerepl� csom�pontok ker�lnek felsorol�sra. Amennyiben a feladatnak nincs megold�sa, akkor a kimeneti f�jl teljesen �res (de l�tre kell hozni). Egy�b esetben a kimeneti f�jl nem tartalmazhat �res sorokat, megjegyz�seket vagy a c�lhoz vezet� �ton k�v�l b�rmi egyebet. A kimeneti f�jlok bin�risan ker�lnek �sszehasonl�t�sra a referencia megold�s kimenet�vel, ez�rt semmilyen egy�b felesleges karaktert se tartalmazhatnak. A sorv�gek mind Windows, mind Unix k�dol�s szerint elfogadhat�ak. A program standard outputja nem ker�l feldolgoz�sra, ez�rt ott tetsz�leges megjegyz�seket megjelen�thet.
